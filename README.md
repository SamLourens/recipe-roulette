# Recipe roulette

## Setting up postgres
Building a postgres image from the root folder:

``` docker image build -t <image_name> .```

Running the image as a container:

```docker run -d --name <container_name> -p <mapped_port_on_localhost>:5432 <image_name>```

Inside the docker container port ``5432`` is automatically opened for the postgres server. You can map it to a port on your localhost by replacing ```<mapped_port_on_localhost>``` in the above command with a port of your choosing.


Running the image as a container with a volume:
```docker run -d --name <image_name> -p <mapped_port_on_localhost>:5432 --volume <volume_name>:var/lib/postgresql/data <container_name>```


### Nice  to know commands:
* ```docker exec -it <container_name> bash ``` : this opens a command line inside your container
* ```psql -U <user> <database>``` : log into the postgres whilst in your container
* ```psql -h localhost -p <mapped_port_on_localhost> -U <user> <database> ``` : log into the postgres server in your container (you have to have postgresql installed on your machine)