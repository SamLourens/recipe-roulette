package recipe.roulette;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecipeRouletteApplication {

    public static void main(String[] args) {
        SpringApplication.run(RecipeRouletteApplication.class, args);
    }

}
