package recipe.roulette.repository;

import org.springframework.data.repository.CrudRepository;
import recipe.roulette.model.Recipe;

import java.util.List;
import java.util.Optional;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {

    void delete(Recipe deleted);

    List<Recipe> findAll();

    Optional<Recipe> findById(Long id);

    Recipe save(Recipe persisted);

}
