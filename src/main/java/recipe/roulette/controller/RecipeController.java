package recipe.roulette.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import recipe.roulette.model.Recipe;
import recipe.roulette.repository.RecipeRepository;

import java.util.List;

@RestController
@RequestMapping("/api/recipe")
public class RecipeController {

    @Autowired
    private RecipeRepository recipeRepository;

    @GetMapping("/all")
    public List<Recipe> findAllRecipes() {
        return recipeRepository.findAll();
    }

    @PostMapping
    public Recipe saveRecipe(@Validated @RequestBody Recipe recipe) {
        return recipeRepository.save(recipe);
    }
}
