-- liquibase formatted sql

-- changeset sam:1
create table recipes (
    id int,
    name varchar(50),
    description varchar(250)
);
-- rollback drop table recipes

-- changeset sam:2
create table ingredients (
    ingredient varchar(50),
    recipe_id int
);
