package recipe.roulette.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;
import recipe.roulette.config.PersistenceConfig;
import recipe.roulette.model.Recipe;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(
        classes = {PersistenceConfig.class},
        loader = AnnotationConfigContextLoader.class)
@Transactional
@ActiveProfiles("test")
public class RecipeRepositoryTest {

    @Resource
    private RecipeRepository recipeRepository;

    @Test
    public void testSaveRecipe() {
        Recipe recipe = new Recipe();
        recipe.setName("Some Recipe");
        recipeRepository.save(recipe);
        assertEquals("Some Recipe", recipeRepository.findById(1l).get().getName());
        // Todo: use JPA instead of the repository. You're now testing the repository's findById() as well
    }

    @Test
    public void testFindAllRecipes() {
        // Todo: implement
    }

    @Test
    public void testFindRecipeById() {
        // Todo: implement
    }

    @Test
    public void testDeleteRecipe() {
        // Todo: implement
    }
}